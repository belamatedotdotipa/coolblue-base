
import Foundation
import UIKit

final class CoolBlueNavigationController: UINavigationController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        topViewController?.preferredStatusBarStyle ?? .default
    }

    func setBlueAppearance() {
        navigationBar.standardAppearance = .blue
        navigationBar.scrollEdgeAppearance = .blue
        navigationBar.tintColor = .white
    }

}

fileprivate extension UINavigationBarAppearance {

private static var baseAppearance: UINavigationBarAppearance {
    let appearance = UINavigationBarAppearance()
    appearance.configureWithDefaultBackground()
    appearance.shadowImage = nil
    appearance.shadowColor = .clear
    return appearance
}

static let blue: UINavigationBarAppearance = {
    let appearance = baseAppearance
    appearance.backgroundColor = #colorLiteral(red: 0.2470588235, green: 0.5529411765, blue: 0.8666666667, alpha: 1)

    appearance.titleTextAttributes = [
        .foregroundColor: UIColor.white,
    ]

    let button = UIBarButtonItemAppearance(style: .plain)
    button.normal.titleTextAttributes = [
        .foregroundColor: UIColor.white
    ]
    appearance.buttonAppearance = button
    return appearance
}()
}
