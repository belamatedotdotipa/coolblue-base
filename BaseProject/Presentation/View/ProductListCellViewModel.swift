import Foundation
import UIKit

struct ProductListCellViewModel {
    
    let product: Product
    
    
    init(product: Product) {
        self.product = product
    }
    
    var name: String { product.productName }
    var imageURL: URL? { URL(string: product.productImage) }
    var rating: Double { (product.reviewInformation.reviewSummary.reviewAverage/2) }
    var ratingCount: String { String(product.reviewInformation.reviewSummary.reviewCount) + " reviews"}
    var usps: [String] { product.usPS.map{ usp in
        " • "+usp
        }
    }
    var price: String { String(format: "%.2f", product.salesPriceIncVat) }
    var deliveryInfo: NSMutableAttributedString? {
        let checkMarkAttachment = NSTextAttachment()
        checkMarkAttachment.image = #imageLiteral(resourceName: "tick").withTintColor(#colorLiteral(red: 0, green: 0.5215686275, blue: 0, alpha: 1))
        checkMarkAttachment.bounds = CGRect(
                                            x: 0,
                                            y: -1.5,
                                            width: checkMarkAttachment.image!.size.width/2,
                                            height: checkMarkAttachment.image!.size.height/2
                                            )
        
        let imageString = NSAttributedString(attachment: checkMarkAttachment)
        let fullString = NSMutableAttributedString()
        fullString.append(imageString)
        fullString.append(NSAttributedString(string: " Morgen in huis"))
        
        return product.nextDayDelivery ? fullString : nil }
}
