//
//  ProductListCell.swift
//  BaseProject
//
//  Created by Máté on 04/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import UIKit
import SnapKit

class ProductListCell: UITableViewCell {
    
    lazy var productImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.setContentHuggingPriority(.defaultLow, for: .vertical)
        return imageView
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica-Bold", size: 16)
        label.textColor = #colorLiteral(red: 0, green: 0.5647058824, blue: 0.8901960784, alpha: 1)
        return label
    }()
    
    private lazy var ratingStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.contentMode = .left
        stackView.spacing = 3
        return stackView
    }()
    
    lazy var ratingCountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica-Light", size: 13)
        label.textColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
        return label
    }()
    
    private lazy var uspStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica-Bold", size: 15)
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return label
    }()
    
    lazy var deliveryInfoLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica-Bold", size: 13)
        label.textColor = #colorLiteral(red: 0, green: 0.5215686275, blue: 0, alpha: 1)
        return label
    }()
    
    
    private lazy var innerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [priceLabel, deliveryInfoLabel])
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var cartButton: UIButton = {
        let button = UIButton()
        button.layer.shadowColor = #colorLiteral(red: 0, green: 0.6196078431, blue: 0, alpha: 1)
        button.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = 4
        button.backgroundColor = #colorLiteral(red: 0.003921568627, green: 0.7098039216, blue: 0.01176470588, alpha: 1)
        button.setImage(UIImage(named: "cart_pdf"), for: .normal)
        button.snp.makeConstraints {
            $0.height.equalTo(40)
            $0.width.equalTo(64)
        }
        return button
    }()
    
    private lazy var bottomContainerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [innerStackView,cartButton])
        
        stackView.axis = .horizontal
        return stackView
    }()
    
    private lazy var rightContainerView: UIView = {
        let view = UIView()
        view.addSubview(nameLabel)
        nameLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        view.addSubview(ratingStackView)
        ratingStackView.snp.makeConstraints {
            $0.top.equalTo(nameLabel.snp.bottom).offset(10)
            $0.leading.equalTo(nameLabel)
        }
        
        view.addSubview(ratingCountLabel)
        ratingCountLabel.snp.makeConstraints {
            $0.leading.equalTo(ratingStackView.snp.trailing).offset(5)
            $0.centerY.equalTo(ratingStackView)
        }
        
        view.addSubview(uspStackView)
        uspStackView.snp.makeConstraints {
            $0.top.equalTo(ratingStackView.snp.bottom).offset(10)
            $0.leading.trailing.equalTo(nameLabel)
        }
        
        view.addSubview(bottomContainerStackView)
        bottomContainerStackView.snp.makeConstraints {
            $0.top.equalTo(uspStackView.snp.bottom).offset(5)
            $0.leading.trailing.equalTo(nameLabel)
            $0.bottom.equalToSuperview()
        }
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        clipsToBounds = true
        
        contentView.addSubview(productImageView)
        contentView.addSubview(rightContainerView)
        productImageView.snp.makeConstraints {
            $0.height.equalTo(140)
            $0.width.equalTo(productImageView.snp.height).inset(20)
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().inset(10)
        }
        
        rightContainerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(20)
            $0.leading.equalTo(productImageView.snp.trailing)
            $0.trailing.equalToSuperview().inset(20)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func makeUSPLabels(for USPs: [String]) {
        uspStackView.arrangedSubviews.forEach{$0.removeFromSuperview()}
        USPs.forEach { usp in
            let label = UILabel()
            label.textColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
            label.font = UIFont(name: "Helvetica-Light", size: 13)
            label.text = usp
            label.numberOfLines = 0
            uspStackView.addArrangedSubview(label)
        }
    }
    
    func makeRatingStars(for rating: Double) {
        ratingStackView.arrangedSubviews.forEach{$0.removeFromSuperview()}
        var remaingRating = rating
        for _ in 1...5 {
            let imageView = UIImageView()
            imageView.snp.makeConstraints { $0.size.equalTo(15) }
            remaingRating -= 1
            var starImage: UIImage
            switch remaingRating {
            case 0...:
                starImage = #imageLiteral(resourceName: "star_full")
            case -1...0:
                starImage = #imageLiteral(resourceName: "star_half")
            default:
                starImage = #imageLiteral(resourceName: "star_empty")
            }
            imageView.image = starImage
            ratingStackView.addArrangedSubview(imageView)
        }
    }
}
