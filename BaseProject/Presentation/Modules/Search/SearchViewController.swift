
import UIKit
import Kingfisher

class SearchViewController: UITableViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var searchController: UISearchController = {
        let search = UISearchController(searchResultsController: nil)
        search.obscuresBackgroundDuringPresentation = false
        DispatchQueue.main.async {
            search.searchBar.searchTextField.attributedPlaceholder = NSAttributedString(
                string: "Zoeken naar...",
                attributes: [
                    NSAttributedString.Key.font: UIFont(name: "Helvetica-Light", size: 14) as Any
            ])
            search.searchBar.searchTextField.backgroundColor = .white
            search.searchBar.barTintColor = .clear
            
        }
        search.searchBar.barTintColor = #colorLiteral(red: 0.2470588235, green: 0.5529411765, blue: 0.8666666667, alpha: 1)        
        
        if let glassIconView = search.searchBar.searchTextField.leftView as? UIImageView {
            
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = #colorLiteral(red: 0.2470588235, green: 0.5529411765, blue: 0.8666666667, alpha: 1)
        }
        return search
    }()
    
    private var searchTerm = String()
    
    private let presenter: SearchPresenter
    
    // MARK: - Init
    init(presenter: SearchPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "cb_logo"))
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        navigationItem.searchController = searchController
        searchController.searchResultsUpdater = self
        
        tableView.register(ProductListCell.self, forCellReuseIdentifier: "ProductListCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 250
        tableView.allowsSelection = false
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2470588235, green: 0.5529411765, blue: 0.8666666667, alpha: 1)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func setSerchTerm() {
        presenter.searchTerm = self.searchTerm
    }
    
}

extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        self.searchTerm = text
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(setSerchTerm), object: nil)
        self.perform(#selector(setSerchTerm), with: nil, afterDelay: 0.4)
    }
}

extension SearchViewController: SearchView {
    func setProducts() {
        tableView.reloadData()
        if !presenter.productList.isEmpty {
            presenter.nextPage += 1
        }
    }
}

extension SearchViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return presenter.productList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductListCell", for: indexPath) as? ProductListCell else { fatalError("Dequeuing cell failed")}
        guard !presenter.productList.isEmpty else { return cell}
        let viewModel = presenter.productList[indexPath.row]
        
        if let imageUrl = viewModel.imageURL {
            cell.productImageView.kf.setImage(with: imageUrl, completionHandler: { _ in
                cell.setNeedsLayout()
            })
        }
        
        cell.nameLabel.text = viewModel.name
        cell.makeUSPLabels(for: viewModel.usps)
        cell.makeRatingStars(for: viewModel.rating)
        cell.ratingCountLabel.text = viewModel.ratingCount
        cell.priceLabel.text = viewModel.price
        cell.deliveryInfoLabel.attributedText = viewModel.deliveryInfo
        return cell        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == tableView.numberOfRows(inSection: 0)-9 {
            presenter.search(for: presenter.searchTerm, page: presenter.nextPage)
        }
    }
}


