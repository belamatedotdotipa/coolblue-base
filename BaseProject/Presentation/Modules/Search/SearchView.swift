//
//  View.swift
//  BaseProject
//
//  Created by Máté on 02/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import Foundation

protocol SearchView: View {
    func setProducts()
}
