import Foundation
import PromiseKit

final class SearchPresenter {
    
    var searchTerm = String() {
        didSet {
            nextPage = 1
            search(for: searchTerm)
            productList = []
        }
    }
    
    var nextPage = 1
    var numberOfPages: Int?
    var isLoading = false
    
    var productList = [ProductListCellViewModel]() {
        didSet {
            view?.setProducts()
        }
    }
    
    weak var view: SearchView?
    private let coordinator: AppCoordinator
    private let firstRepository: ProductRepository
    
    init(coordinator: AppCoordinator, firstRepository: ProductRepository) {
        self.coordinator = coordinator
        self.firstRepository = firstRepository
    }
    
    func search(for term: String, page: Int = 1) {
        if !isLoading && nextPage <= numberOfPages ?? 1 {
            isLoading = true
            firstRepository.getProductList(for: term, page: "\(page)")
            .done { response in
                    self.numberOfPages = response.pageCount
                    
                    self.productList += response.products.map { product in
                        ProductListCellViewModel(product: product)
                        
                    }
                    self.isLoading = false
            }
            .catch { error in
                print(error)
            }
        }
    }

}
