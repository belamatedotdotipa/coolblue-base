//
//  AppCoordinator.swift
//  BaseProject
//
//  Created by Máté on 02/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

final class AppCoordinator {

private let dependencyContainer: DependencyContainer
private let navigationController: CoolBlueNavigationController

init(dependencyContainer: DependencyContainer,
     navigationController: CoolBlueNavigationController) {
    self.dependencyContainer = dependencyContainer
    self.navigationController = navigationController
}

func start() {
    let presenter = SearchPresenter(
        coordinator: self,
        firstRepository: dependencyContainer.firstRepository
    )
    let viewController = SearchViewController(presenter: presenter)
    presenter.view = viewController
    navigationController.setBlueAppearance()
    navigationController.setViewControllers([viewController], animated: false)
    }
}
