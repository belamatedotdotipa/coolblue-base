//
//  JSONDecoder+Extensions.swift
//  BaseProject
//
//  Created by Máté on 02/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import Foundation

extension JSONDecoder {

    static var `default`: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .secondsSince1970
        return decoder
    }
}
