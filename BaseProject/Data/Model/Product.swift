//
//  FirstModel.swift
//  BaseProject
//
//  Created by Máté on 02/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import Foundation

// MARK: - Product
struct Product: Codable {
    let productName: String
    let reviewInformation: ReviewInformation
    let usPS: [String]
    let availabilityState: Int
    let salesPriceIncVat: Double
    let productImage: String
    let nextDayDelivery: Bool

    enum CodingKeys: String, CodingKey {
        case productName, reviewInformation
        case usPS = "USPs"
        case availabilityState, salesPriceIncVat, productImage, nextDayDelivery
    }
}

// MARK: - ReviewInformation
struct ReviewInformation: Codable {
    let reviewSummary: ReviewSummary
}

// MARK: - ReviewSummary
struct ReviewSummary: Codable {
    let reviewAverage: Double
    let reviewCount: Int
}
