//
//  ProductList.swift
//  BaseProject
//
//  Created by Máté on 05/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import Foundation

// MARK: - Products
struct ProductList: Codable {
    let products: [Product]
    let currentPage, pageSize, totalResults, pageCount: Int
}
