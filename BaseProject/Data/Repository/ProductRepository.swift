
import PromiseKit

final class ProductRepository {

    private let network: Network

    init(network: Network) {
        self.network = network
    }

    func getProductList(for searchTerm: String, page: String) -> Promise<ProductList> {
        
        let params = [
            "query": searchTerm,
            "page": page
        ]

        let request: Promise<ProductList> = network.request(Route(.get, .product(.search), with: params))
        return request
    }
}
