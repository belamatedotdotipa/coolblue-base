//
//  Network.swift
//  BaseProject
//
//  Created by Máté on 02/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import Foundation
import PromiseKit

protocol Network {
    func request<Type: Decodable>(_ route: Route) -> Promise<Type>
    func request(_ route: Route) -> Promise<Void>
}
