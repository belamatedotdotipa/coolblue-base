//
//  NetworkManager.swift
//  BaseProject
//
//  Created by Máté on 02/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import Alamofire
import Foundation
import PromiseKit

enum NetworkError: Error {
    case serverError(ServerError)
    case connectionTimeout
    case notConnectedToInternet
    case serverConnectionFailed
    case other(AFError)
}


struct ServerError {
    var statusCode: Int
    var data: Data?
}

final class NetworkManager: Network {

    private let session: Session
    private let decoder: JSONDecoder

    init(session: Session,
         decoder: JSONDecoder) {
        self.session = session
        self.decoder = decoder
    }

    private func process(error: AFError, with responseData: Data?) -> NetworkError {
        switch error {
        case .responseValidationFailed(let reason):
            switch reason {
            case .unacceptableStatusCode(let statusCode):
                let serverError = ServerError(statusCode: statusCode, data: responseData)
                return NetworkError.serverError(serverError)

            default: break
            }

        case .sessionTaskFailed(let _error as NSError):
            switch _error.code {
            case NSURLErrorTimedOut:
                return NetworkError.connectionTimeout

            case NSURLErrorCannotConnectToHost:
                return NetworkError.serverConnectionFailed

            case NSURLErrorNotConnectedToInternet:
                return NetworkError.notConnectedToInternet

            default: break
            }

        default: break
        }

        return NetworkError.other(error)
    }

    func request<Type: Decodable>(_ route: Route) -> Promise<Type> {
        Promise { seal in
            session
                .request(route)
                .validate()
                .responseData { response in
                    switch response.result {
                    case .success(let data):
                        do {
                            seal.fulfill(try self.decoder.decode(Type.self, from: data))
                        } catch let error {
                            seal.reject(error)
                        }

                    case .failure(let error):
                        let networkError = self.process(error: error, with: response.data)
                        seal.reject(networkError)
                    }
            }
        }
    }

    func request(_ route: Route) -> Promise<Void> {
        Promise { seal in
            session
                .request(route)
                .validate()
                .response { response in
                    switch response.result {
                    case .success:
                        seal.fulfill(())

                    case .failure(let error):
                        let networkError = self.process(error: error, with: response.data)
                        seal.reject(networkError)
                    }
            }
        }
    }
}
