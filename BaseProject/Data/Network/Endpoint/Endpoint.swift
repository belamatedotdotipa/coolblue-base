//
//  Endpoint.swift
//  BaseProject
//
//  Created by Máté on 02/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import Foundation

struct Endpoint {

    private var base: URL
    private var path: String

    init(base: URL, path: String) {
        self.base = base
        self.path = path
    }

    var url: URL {
        return base.appendingPathComponent(path)
    }
}
