//
//  Endpoint+[Extension].swift
//  BaseProject
//
//  Created by Máté on 02/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import Foundation

extension Endpoint {

    static func product(_ product: Endpoint.Product) -> Endpoint {
        product.endpoint
    }

    enum Product {
        case search

        var endpoint: Endpoint {
            switch self {
            case .search:
                return Endpoint(base: URL(string: "https://bdk0sta2n0.execute-api.eu-west-1.amazonaws.com")!, path: "mobile-assignment/search")
            }
        }
    }
}
