//
//  DependencyContainer.swift
//  BaseProject
//
//  Created by Máté on 02/11/2020.
//  Copyright © 2020 Máté. All rights reserved.
//

import Foundation
import Alamofire

final class DependencyContainer {


    // MARK: - Basic network

    private lazy var session = Session(
        configuration: .default
    )

    private lazy var networkManager = NetworkManager(
        session: session,
        decoder: JSONDecoder.default
    )

    // MARK: - Repositories

    lazy var firstRepository = ProductRepository(network: networkManager)

}
